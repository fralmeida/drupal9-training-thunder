# Install

lando drush site:install --existing-config --yes && lando drush cim --yes
lando drush upwd admin "admin"



# List of all resources

http://drupal9-thunder.lndo.site/jsonapi

curl \
  --header 'Accept: application/vnd.api+json' \
  http://drupal9-thunder.lndo.site/jsonapi/node/article

  curl \
  --header 'Accept: application/vnd.api+json' \
  http://drupal9-training.lndo.site/jsonapi/node/article?page[limit]=1


curl \
  --header 'Accept: application/vnd.api+json' \
  http://drupal9-thunder.lndo.site/jsonapi/node/article?page[limit]=2



# POST

## Curl
Assume your data is in the file payload.json.

curl \
    --user admin:admin \
    --header 'Accept: application/vnd.api+json' \
    --header 'Content-type: application/vnd.api+json' \
    --request POST http://drupal9-thunder.lndo.site/jsonapi/node/article \
    --data-binary @payload_demo1.json

This example on thunder, taxonomy is mandatory !!!

